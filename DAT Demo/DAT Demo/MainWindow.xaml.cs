﻿using System.Windows;
using System.Windows.Forms;
using System.IO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data;
using System.Data.SQLite;
using System.Dynamic;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Input;

namespace DAT_Demo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // AIP Report Module
        string selectedSheetName;
        string givenCustomSheetName;
        private static string databaseName = "database.db";
        string tableName;
        bool hasTableLoaded = false;

        List<string> excelColNames = new List<string>();

        // Universal SQLiteConnection
        private static SQLiteConnection sqlConnection = new SQLiteConnection("Data Source='" + databaseName + "';Version=3;New=True;Compress=True;");

        private static SQLiteDataAdapter sqlDataAdapter;

        private static SQLiteCommandBuilder sqlCommandBuilder;

        // Fixity Parsing Module
        FixityParsing fixityParsing;


        public MainWindow()
        {
            InitializeComponent();
            fillExistingTableNamesCB();
        }

        /// <summary>
        /// ---------------------------------------------------------------------------------------------------------------------------------------------
        /// ----                                           ----------------------------                                                              ----
        /// ----                                           ----------------------------                                                              ----
        /// -----------------------------------------------AIP REPORT MODULE BEGINS-----------------------------------------------
        /// -----------------------------------------------AIP REPORT MODULE BEGINS-----------------------------------------------
        /// -----------------------------------------------AIP REPORT MODULE BEGINS-----------------------------------------------
        /// ----                                           ----------------------------                                                              ----
        /// ----                                           ----------------------------                                                              ----
        /// ---------------------------------------------------------------------------------------------------------------------------------------------
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 


        // -------------------------POINT 1: Selection on AIP Report Excel File
        private void aipReportSelectionBTN_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openExcelFile = new Microsoft.Win32.OpenFileDialog();
            openExcelFile.Filter = "Excel File (*.xlsx)|*.xlsx";
            if (openExcelFile.ShowDialog() == true)
            {
                aipReportPathTF.Text = openExcelFile.FileName;
                sheetNameCB.ItemsSource = lookForSheetNamesInExcel(aipReportPathTF.Text);
                sheetNameCB.Text = "Select sheet";
            }
            
        }

        // -------------------------POINT 2: Finds the sheet names in selected Excel file and fill the sheet name combo box.
        private List<string> lookForSheetNamesInExcel(string excelFilePath)
        {
            List<string> listOfSheets = new List<string>();
            try
            {
                OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + excelFilePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
                conn.Open();
                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                foreach (DataRow row in dtSheet.Rows)
                {
                    if (row["TABLE_NAME"].ToString().Contains("$"))
                    {
                        string result = row["TABLE_NAME"].ToString().Replace("$", "");
                        listOfSheets.Add(result);
                    }
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error looking for sheet names in uploaded Excel. " + ex.Message);
            }
            return listOfSheets;
        }

        // -------------------------POINT 3: Reads the sheet selected for the AIP Report Excel File.
        private void readAIPReportBTN_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(this.aipReportPathTF.Text) || sheetNameCB.SelectedIndex == -1 || string.IsNullOrWhiteSpace(givenSheetNameTB.Text))
            {
                System.Windows.MessageBox.Show("Please select an AIP Excel File, it's corresponding sheet from the list of sheet names and give it a custome name.");
            }
            else
            {
                selectedSheetName = sheetNameCB.SelectedItem.ToString();
                givenCustomSheetName = givenSheetNameTB.Text;

                // Setting universal tableName
                tableName = givenCustomSheetName;

                if(excelColNames.Count>0)
                {
                    excelColNames.Clear();
                }

                createTable(tableName, getExcelColumnNames());
                // Possible logic break when user saves data grid view as another table with new table name
                List<ExpandoObject> excelData = convertAndReturnExcelDataForDB(getExcelColumnNames());
                insertExcelDataIntoDB(excelData);
                fillExistingTableNamesCB();
            }
        }

        private void createTable(string tableName, List<string> excelColNames)
        {
            try
            {
                SQLiteConnection localConn = new SQLiteConnection("Data Source='" + databaseName + "';Version=3;New=True;Compress=True;");
                localConn.Open();
                SQLiteCommand sqlCommand = localConn.CreateCommand();
                //System.Windows.MessageBox.Show("Create table: tableName: " + tableName);
                string createString = "create table '" + tableName + "' (";
                int count = excelColNames.Count;
                foreach (string colName in excelColNames)
                {
                    if (count >= 1)
                    {
                        createString += colName;
                        if (colName == "ID")
                        {
                            createString += " int";
                        }
                        else
                        {
                            createString += " varchar(100)";
                        }
                        count--;
                        if (count >= 1)
                        {
                            createString += ", ";
                        }

                    }
                }
                createString += ", Primary Key(ID));";
                //System.Windows.MessageBox.Show("Create string: " + createString);
                sqlCommand.CommandText = createString;
                createString = string.Empty;
                //System.Windows.MessageBox.Show("Create string after string.empty: " + createString);
                sqlCommand.ExecuteNonQuery();
                sqlCommand.Dispose();
                localConn.Close();
                System.Windows.MessageBox.Show("Table '" + tableName + "' created!");
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private List<string> getExcelColumnNames()
        {
            var sheetName = sheetNameCB.SelectedItem.ToString();
            string query = "select * from [" + sheetName + "$]";
            OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.aipReportPathTF.Text + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
            OleDbDataAdapter adapter = new OleDbDataAdapter(query, conn);
            DataTable dtExcel = new DataTable();
            adapter.Fill(dtExcel);
            int numOfCols = dtExcel.Columns.Count;
            foreach (DataColumn col in dtExcel.Columns)
            {
                excelColNames.Add(col.ColumnName);
            }
            return excelColNames;
        }

        private List<ExpandoObject> convertAndReturnExcelDataForDB(List<string> excelColNames)
        {
            OleDbConnection conn;
            conn = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + this.aipReportPathTF.Text + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"");
            conn.Open();
            OleDbCommand command = new OleDbCommand();
            command.Connection = conn;
            command.CommandText = "select * from [" + sheetNameCB.SelectedItem.ToString() + "$]";
            OleDbDataReader reader = command.ExecuteReader();

            List<ExpandoObject> dynamicAIPObjList = new List<ExpandoObject>();
            while (reader.Read())
            {
                dynamic expandoObj = new ExpandoObject();
                foreach (string colNameToPropName in excelColNames)
                {
                    AddProperty(expandoObj, colNameToPropName, reader[colNameToPropName].ToString());
                }
                dynamicAIPObjList.Add(expandoObj);
            }
            //System.Windows.MessageBox.Show("No of items in dynamicObjList: " + dynamicAIPObjList.Count);
            
            reader.Close();
            conn.Close();
            return dynamicAIPObjList;
        }

        public static void AddProperty(ExpandoObject expando, string propertyName, object propertyValue)
        {
            // ExpandoObject supports IDictionary so we can extend it like this
            var expandoDict = expando as IDictionary<string, object>;
            if (expandoDict.ContainsKey(propertyName))
                expandoDict[propertyName] = propertyValue;
            else
                expandoDict.Add(propertyName, propertyValue);
        }

        private void insertExcelDataIntoDB(List<ExpandoObject> expandoList)
        {
            try
            {
                //sqlConnection.Open();
                SQLiteConnection localConn = new SQLiteConnection("Data Source='" + databaseName + "';Version=3;New=True;Compress=True;");
                localConn.Open();
                SQLiteCommand sqlCommand = localConn.CreateCommand();
                //System.Windows.MessageBox.Show("Inserting excel data in to Database name: " + databaseName + ", Table name: " + tableName);

                //Retreiving db col names
                string retrieveColNames = "select * from '" + tableName + "'";
                List<string> dbColNames = new List<string>();
                SQLiteDataAdapter sqlLocalDA = new SQLiteDataAdapter(retrieveColNames, localConn);
                DataTable dtDB = new DataTable();
                sqlLocalDA.Fill(dtDB);
                foreach (DataColumn col in dtDB.Columns)
                {
                    dbColNames.Add(col.ColumnName);
                }
                List<string> insertList = new List<string>();

                string testString = "";
                //Begin inserting expando objects
                foreach (ExpandoObject obj in expandoList)
                {
                    testString = "insert into '" + tableName + "' (";
                    int count = dbColNames.Count;
                    foreach (string colName in dbColNames)
                    {
                        if (count >= 1)
                        {
                            testString += colName;
                            count--;
                            if (count >= 1)
                            {
                                testString += ", ";
                            }

                        }
                    }
                    testString += ") values (";

                    int countProperties = ((IDictionary<string, object>)obj).Count;
                    foreach (var property in (IDictionary<string, object>)obj)
                    {
                        if (countProperties >= 1)
                        {
                            testString += "'" + property.Value + "'";
                            countProperties--;
                            if (countProperties >= 1)
                            {
                                testString += ",";
                            }
                        }

                    }
                    testString += ");";
                    //System.Windows.MessageBox.Show("TestString: " + testString);
                    insertList.Add(testString);
                }

                foreach (string insertQuery in insertList)
                {
                    //System.Windows.MessageBox.Show("Insert Query: " + insertQuery);

                    try
                    {
                        sqlCommand.CommandText = insertQuery;
                        sqlCommand.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message);
                    }

                }

                putDataInDataGrid();
                existingTableNamesCB.Text = "Select table";
                localConn.Close();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void putDataInDataGrid()
        {
            try
            {
                //System.Windows.MessageBox.Show("Pulling data from database: " + databaseName + ", table: " + tableName);

                //SQLiteConnection conn = new SQLiteConnection("Data Source='" + databaseName + "';Version=3;New=True;Compress=True;");
                //conn.Open();
                sqlConnection.Open();
                SQLiteCommand sqlCommand = sqlConnection.CreateCommand();
                sqlCommand.CommandText = "select * from '" + tableName + "'";
                sqlCommand.ExecuteNonQuery();
                sqlDataAdapter = new SQLiteDataAdapter(sqlCommand.CommandText, sqlConnection);
                sqlCommandBuilder = new SQLiteCommandBuilder(sqlDataAdapter);
                DataTable dt = new DataTable();
                sqlDataAdapter.Fill(dt);
                dgAIP.ItemsSource = dt.DefaultView;
                hasTableLoaded = true;
                sqlConnection.Close();
                loadColNameCB();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Weird error here "+ex.Message);
            }
        }

        private void addColumnBTN_Click(object sender, RoutedEventArgs e)
        {
            if (hasTableLoaded)
            {
                if (!string.IsNullOrWhiteSpace(addColumnTB.Text))
                {
                    try
                    {
                        sqlConnection.Open();
                        SQLiteCommand command = sqlConnection.CreateCommand();
                        command.CommandText = "alter table '" + tableName + "' add column '" + addColumnTB.Text + "' varchar(100);";
                        command.ExecuteNonQuery();
                        command = sqlConnection.CreateCommand();
                        command.CommandText = "select * from '" + tableName + "'";
                        sqlDataAdapter = new SQLiteDataAdapter(command.CommandText, sqlConnection);
                        sqlCommandBuilder = new SQLiteCommandBuilder(sqlDataAdapter);
                        DataTable sqlDT = new DataTable();
                        sqlDataAdapter.Fill(sqlDT);
                        dgAIP.ItemsSource = sqlDT.DefaultView;
                        loadColNameCB();
                        sqlConnection.Close();
                        System.Windows.MessageBox.Show("Column '" + addColumnTB.Text + "' added.");
                    }
                    catch (Exception ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message);
                    }
                    addColumnTB.Text = string.Empty;
                }
                else
                {
                    System.Windows.MessageBox.Show("Give a column name.");
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Load a table from existing tables or upload a new AIP Excel file.");
            }
            
        }


        private void deleteColBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (columnNameDeleteCB.SelectedIndex != -1)
                {
                    string colNameSelected = columnNameDeleteCB.SelectedItem.ToString();

                    List<string> colNames = new List<string>();
                    DataTable table = new DataTable();
                    table = ((DataView)dgAIP.ItemsSource).ToTable();

                    foreach (DataColumn col in table.Columns)
                    {
                        if (col.ToString() != colNameSelected)
                        {
                            colNames.Add(col.ToString());
                        }
                    }

                    sqlConnection.Open();

                    string oldTableName = tableName;
                    string newTableName = tableName + " 1";

                    //string newTableName = tableName + " " + localDate.ToString(culture);
                    //System.Windows.MessageBox.Show("Old tableName: " + oldTableName + ", new tableName : " + newTableName);

                    string createString = "create table '" + newTableName + "' (";
                    int count = colNames.Count;
                    foreach (string colName in colNames)
                    {
                        if (count >= 1)
                        {
                            createString += colName;
                            if (colName == "ID")
                            {
                                createString += " int";
                            }
                            else
                            {
                                createString += " varchar(100)";
                            }
                            count--;
                            if (count >= 1)
                            {
                                createString += ", ";
                            }

                        }
                    }
                    createString += ", Primary Key (ID));";

                    //System.Windows.MessageBox.Show("New table: " + createString);

                    SQLiteCommand sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = createString;
                    sqlCommand.ExecuteNonQuery();

                    //Initiating table data transfer
                    string dataTransfer = "insert into '" + newTableName + "' select ";
                    count = colNames.Count;
                    foreach (string colName in colNames)
                    {
                        if (count >= 1)
                        {
                            dataTransfer += colName;
                            count--;
                            if (count >= 1)
                            {
                                dataTransfer += ", ";
                            }
                        }
                    }
                    dataTransfer += " from '" + oldTableName + "';";

                    sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = dataTransfer;
                    sqlCommand.ExecuteNonQuery();

                    //System.Windows.MessageBox.Show("Data Transfer: " + dataTransfer);

                    //Clearing data grid
                    dgAIP.ItemsSource = null;

                    //Deleting old table
                    sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = "drop table '" + oldTableName + "'";
                    sqlCommand.ExecuteNonQuery();

                    //Renaming new table to old table to prevent confusing user when reusing the table
                    sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = "alter table '" + newTableName + "' rename to '" + oldTableName + "'";
                    sqlCommand.ExecuteNonQuery();

                    tableName = oldTableName;

                    //Repopulating datagrid with new table
                    sqlCommand = sqlConnection.CreateCommand();
                    sqlCommand.CommandText = "select * from '" + tableName + "'";
                    sqlDataAdapter = new SQLiteDataAdapter(sqlCommand.CommandText, sqlConnection);
                    sqlCommandBuilder = new SQLiteCommandBuilder(sqlDataAdapter);
                    DataTable tempTable = new DataTable();
                    sqlDataAdapter.Fill(tempTable);
                    dgAIP.ItemsSource = tempTable.DefaultView;
                    loadColNameCB();
                    sqlConnection.Close();
                    columnNameDeleteCB.Text = "Select column";
                    System.Windows.MessageBox.Show("Successfully deleted: " + colNameSelected);
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void loadColNameCB()
        {
            if (hasTableLoaded)
            {
                DataTable table = new DataTable();
                table = ((DataView)dgAIP.ItemsSource).ToTable();
                List<string> colNames = new List<string>();
                foreach (DataColumn col in table.Columns)
                {
                    if (col.ToString() != "ID")
                    {
                        colNames.Add(col.ToString());
                    }
                }
                columnNameDeleteCB.ItemsSource = colNames;
            }
        }

        private void fillExistingTableNamesCB()
        {
            try
            {
                SQLiteConnection localConn = new SQLiteConnection("Data Source='" + databaseName + "';Version=3;New=True;Compress=True;");
                localConn.Open();
                List<string> tableList = new List<string>();
                DataTable dt = localConn.GetSchema("Tables");
                foreach (DataRow row in dt.Rows)
                {
                    tableList.Add((string)row[2]);
                }
                localConn.Close();
                if (tableList.Count >0)
                {
                    existingTableNamesCB.ItemsSource = tableList;
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        private void loadTableBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (existingTableNamesCB.SelectedIndex != -1)
                {
                    sqlConnection.Open();
                    SQLiteCommand command = sqlConnection.CreateCommand();
                    command.CommandText = "select * from '"+existingTableNamesCB.SelectedItem.ToString()+"';";
                    command.ExecuteNonQuery();

                    sqlDataAdapter = new SQLiteDataAdapter(command);
                    DataTable dt = new DataTable();
                    sqlDataAdapter.Fill(dt);
                    dgAIP.ItemsSource = dt.DefaultView;
                    sqlConnection.Close();
                    hasTableLoaded = true;

                    tableName = existingTableNamesCB.SelectedItem.ToString();
                    aipReportPathTF.Text = "AIP Excel Report Location";
                    sheetNameCB.Text = "Select sheet";
                    givenSheetNameTB.Text = string.Empty;
                    
                    System.Windows.MessageBox.Show("Successfully loaded table: " + existingTableNamesCB.SelectedItem.ToString());
                    loadColNameCB();
                }
                else
                {
                    System.Windows.MessageBox.Show("Please select a table to load.");
                }
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }


        //private void deleteTableBTN_Click(object sender, RoutedEventArgs e)
        //{

        //}

        private void submitBTN_Click(object sender, RoutedEventArgs e)
        {
            if (hasTableLoaded)
            {
                try
                {
                    // For situations when the user hasn't uploaded an Excel 
                    // file but just started the application and loaded a previously
                    // created table. This will initialize the necessary objects for CRUD operations.
                    if (sqlDataAdapter == null)
                    {
                        sqlConnection.Open();
                        SQLiteCommand command = sqlConnection.CreateCommand();
                        command.CommandText = "select * from '" + tableName + "'";
                        sqlDataAdapter = new SQLiteDataAdapter(command.CommandText, sqlConnection);
                        sqlCommandBuilder = new SQLiteCommandBuilder(sqlDataAdapter);
                        sqlDataAdapter.Update((dgAIP.ItemsSource as DataView).Table);
                        sqlConnection.Close();
                    }
                    else
                    {
                        sqlDataAdapter.Update((dgAIP.ItemsSource as DataView).Table);
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
            else
            {
                System.Windows.MessageBox.Show("Cannot save changes. Load a table from existing tables or upload a new AIP Excel file.");
            }
            
        }


        private void exportTableToExcelBTN_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dgAIP.SelectAllCells();
                dgAIP.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                ApplicationCommands.Copy.Execute(null, dgAIP);
                string clipboardResult = (string)System.Windows.Clipboard.GetData(System.Windows.DataFormats.CommaSeparatedValue);
                StreamWriter streamObj = new StreamWriter("C:\\Users\\'" + tableName + "'-export.csv");
                streamObj.WriteLine(clipboardResult);
                streamObj.Close();
                System.Windows.MessageBox.Show("Successfully exported to CSV.");
                dgAIP.UnselectAllCells();
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// ---------------------------------------------------------------------------------------------------------------------------------------------
        /// ----                                           ----------------------------                                                              ----
        /// ----                                           ----------------------------                                                              ----
        /// -----------------------------------------------FIXITY PARSING MODULE -----------------------------------------------
        /// -----------------------------------------------FIXITY PARSING MODULE -----------------------------------------------
        /// -----------------------------------------------FIXITY PARSING MODULE -----------------------------------------------
        /// ----                                           ----------------------------                                                              ----
        /// ----                                           ----------------------------                                                              ----
        /// ---------------------------------------------------------------------------------------------------------------------------------------------
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void selectFixityReportBTN(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog fBD = new FolderBrowserDialog();
            fBD.Description = "Select Fixity Report Folder";
            if (fBD.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fixityFolferPathTB.Text = fBD.SelectedPath;
                string[] files = Directory.GetFiles(fixityFolferPathTB.Text, "*.tsv");
                if (!(files.Length == 0))
                {
                    totalReportsLBL.Text = files.Length + " files";
                }
            }
        }

        private void parseFixityReportsBTN_Click(object sender, RoutedEventArgs e)
        {
            if (!Directory.Exists(fixityFolferPathTB.Text))
            {
                System.Windows.MessageBox.Show("Please Select a Valid path");

            }
            else
            {
                string[] files = Directory.GetFiles(fixityFolferPathTB.Text, "*.tsv");
                if (!(files.Length == 0))
                {
                    fixityParsing = new FixityParsing(fixityFolferPathTB.Text, this);
                    fixityParsing.generateAllFixityReport();
                }
                else
                {
                    System.Windows.MessageBox.Show("Selected folder does not have tsv files");
                }
            }
        }

        private void readFixityReportsBTN_Click(object sender, RoutedEventArgs e)
        {
            if (projectNamesCB.SelectedIndex != -1)
            {
                string projectNameSelected = projectNamesCB.SelectedItem.ToString();
                fixityParsing.printProjectResult(projectNameSelected);
            }
            else
            {
                if (projectNamesCB.Items.Count == 0)
                {
                    System.Windows.MessageBox.Show("Please load a Fixity Report folder and parse it to get the project names.");
                }
                else
                {
                    System.Windows.MessageBox.Show("Please select a Fixity project from the drop down.");
                }
            }
        }

        private void clearDataBTN_Click(object sender, RoutedEventArgs e)
        {
            projectNameTB.Clear();
            totalFilesTB.Clear();
            confirmedFilesTB.Clear();
            movedOrRenamedTB.Clear();
            newFilesTB.Clear();
            changedFilesTB.Clear();
            projectNamesCB.SelectedIndex = -1;
            dgRemovedFilesList.ItemsSource = null;
            dgNewFilesList.ItemsSource = null;
            dgChangedFilesList.ItemsSource = null;
            dgConfirmedFilesList.ItemsSource = null;

            projectNamesCB.Text = "Select Project Name";
        }

    }
}
